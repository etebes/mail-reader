package com.desarrollo.agil.mailreader;

import java.util.concurrent.ThreadPoolExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.support.GenericHandler;
import org.springframework.integration.mail.ImapMailReceiver;
import org.springframework.integration.mail.MailReceiver;
import org.springframework.integration.mail.MailReceivingMessageSource;
import org.springframework.integration.mail.support.DefaultMailHeaderMapper;
import org.springframework.messaging.Message;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 *
 * @author Emiliano
 */
@Configuration
@IntegrationComponentScan
public class MailReaderFlowConfig {

    @Value("${mail.connection.string}")
    private String mailConnectionString;

    @Autowired
    @Qualifier("customMessageHandler")
    private GenericHandler<Message> messageHandler;

    private final Logger logger = LoggerFactory.getLogger(MailReaderFlowConfig.class);

    @Bean
    public AsyncTaskExecutor asyncTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(10);
        threadPoolTaskExecutor.setMaxPoolSize(200);
        threadPoolTaskExecutor.setQueueCapacity(0);
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        return threadPoolTaskExecutor;
    }

    @Bean
    public DefaultMailHeaderMapper defaultMailHeaderMapper() {
        return new DefaultMailHeaderMapper();
    }

    @Bean
    public MailReceiver mailReceiver() {
        ImapMailReceiver imapMailReceiver = new ImapMailReceiver(mailConnectionString);
        imapMailReceiver.setHeaderMapper(defaultMailHeaderMapper());
        return imapMailReceiver;
    }

    @Bean
    @InboundChannelAdapter(value = "testReceiveEmailChannel", poller = @Poller(fixedDelay = "10000", taskExecutor = "asyncTaskExecutor"))
    public MessageSource mailMessageSource(MailReceiver mailReceiver) {
        MailReceivingMessageSource mailReceivingMessageSource = new MailReceivingMessageSource(mailReceiver);
        return mailReceivingMessageSource;
    }

    @Bean
    public IntegrationFlow leerMails() {
        logger.info("flow activado");
        return f -> f
                .channel("testReceiveEmailChannel")
                .handle(messageHandler)
                .channel("nullChannel");
    }

}
