package com.desarrollo.agil.mailreader.handler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.dsl.support.GenericHandler;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 *
 * @author Emiliano
 */
@Component
@Qualifier("customMessageHandler")
public class MessageHandlerImpl implements GenericHandler<Message> {

    private final Logger logger = LoggerFactory.getLogger(MessageHandlerImpl.class);

    @Override
    public Message handle(Message message, Map<String, Object> map) {
        logger.info("HOLA, se ha recibido un mensaje.");
        logger.info(message.getHeaders().get("contentType").toString());
        String messageString = new String((byte[]) message.getPayload());
        logger.info(messageString);
        return message;
    }

}
