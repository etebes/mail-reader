package com.desarrollo.agil.mailreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailreaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailreaderApplication.class, args);
	}
}
